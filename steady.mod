NEURON {
        SUFFIX steady
        USEION ca WRITE ica VALENCE 2
        RANGE ica
}
 
ASSIGNED {
    ica     (mA/cm2)
}
 
BREAKPOINT {
    ica = 1
}