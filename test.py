from neuron import h
import rxd
from matplotlib import pyplot
import time

# Need to have at least one Section or CVode.statistics will crash
s = h.Section()


h.load_file('stdrun.hoc')
extracellular = rxd.Extracellular(-100, -100, -50, 100, 100, 50, dx=4)

ca_ext = rxd.Species(extracellular, name='ca', charge=2, d=1, initial=0)

old_num_threads = rxd.options.num_threads
rxd.options.num_threads = 4
print 'setting num threads', 'was', old_num_threads, 'now', rxd.options.num_threads

h.CVode().active(1)
h.finitialize()

ca_ext.states[5:15, 5:15, :] = 1

# NEURON fact-of-life: always have to call this when manually changing values in a CVode simulation
h.CVode().re_init()

pyplot.figure()
pyplot.imshow(ca_ext.states[:, :, 0].copy(), interpolation='nearest', vmin=0, vmax=1, extent=ca_ext.extent, origin='lower')
pyplot.title('Initial, plane 0')
print 'initial sum: %g' % ca_ext.states.sum()
print 'advancing...'
start = time.time()
h.continuerun(100)
print '... advance took %gs' % (time.time() - start)
print 'final sum: %g' % ca_ext.states.sum()
print 'final max: %g' % ca_ext.states.max()
h.CVode().statistics()

pyplot.figure()
pyplot.imshow(ca_ext.states[:, :, 0], interpolation='nearest', vmin=0, vmax=1, extent=ca_ext.extent, origin='lower')
pyplot.title('t = %g, plane 0' % h.t)
pyplot.show()

