from neuron import h, gui
import rxd
from matplotlib import pyplot
import numpy
import os

# diffusion constant
d = 10

# extracellular spatial discretization (microns)
dx = 10

padding = 50 # (microns)

# load in the morphology c91662.swc, instantiate into the object mycell
h.load_file('import3d.hoc')
cell = h.Import3d_SWC_read()
cell.input('c91662.swc')
i3d = h.Import3d_GUI(cell, 0)
class Cell:
    def extrema(self):
        h.define_shape()
        xlo = ylo = zlo = xhi = yhi = zhi = None
        for sec in self.all:
            n3d = int(h.n3d(sec=sec))
            xs = [h.x3d(i, sec=sec) for i in xrange(n3d)]
            ys = [h.y3d(i, sec=sec) for i in xrange(n3d)]
            zs = [h.z3d(i, sec=sec) for i in xrange(n3d)]
            my_xlo, my_ylo, my_zlo = min(xs), min(ys), min(zs)
            my_xhi, my_yhi, my_zhi = max(xs), max(ys), max(zs)
            if xlo is None:
                xlo, ylo, zlo = my_xlo, my_ylo, my_zlo
                xhi, yhi, zhi = my_xhi, my_yhi, my_zhi
            else:
                xlo, ylo, zlo = min(xlo, my_xlo), min(ylo, my_ylo), min(zlo, my_zlo)
                xhi, yhi, zhi = max(xhi, my_xhi), max(yhi, my_yhi), max(zhi, my_zhi)
        return xlo, ylo, zlo, xhi, yhi, zhi
mycell = Cell()
i3d.instantiate(mycell)

# discretize
for sec in mycell.all:
    sec.nseg = 1 + 10 * int(sec.L / 5)
xlo, ylo, zlo, xhi, yhi, zhi = mycell.extrema()

print 'defining the region'
extracellular = rxd.Extracellular(xlo - padding, ylo - padding, zlo - padding, xhi + padding, yhi + padding, zhi + padding, dx=dx)
print 'defining the species'
ca = rxd.Species(extracellular, d=d, name='ca', charge=2, initial=0)
print 'inserting current'
# insert a steady calcium current
for sec in h.allsec():
    sec.insert('steady')

print 'aa'

def imsave():
    """plot a collapsed version of the extracellular space"""
    print h.t
    #return
    pyplot.figure()
    data = numpy.zeros([extracellular._nx, extracellular._ny])
    for z in xrange(extracellular._nz):
        data += ca.states[:, :, z]
    data = data / float(extracellular._nz)
    print 'max', data.max()
    pyplot.imshow(data, vmin=0, vmax=0.03)
    pyplot.savefig('test2/%04d.png' % (h.t / h.dt))
    pyplot.close()

try:
    os.makedirs('test2')
except:
    pass

print 'num segs =', sum([sec.nseg for sec in h.allsec()])
h.finitialize()
print 'a'
h.dt = 5

while h.t < 1000:
    imsave()
    h.fadvance()