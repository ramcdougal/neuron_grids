#include <pthread.h>

typedef void (*fptr)(void);

typedef struct {
	Grid_node* grid;
	int idx;
} ReactSet;

typedef struct {
	ReactSet* onset;
	ReactSet* offset;
} ReactGridData;

typedef struct {
        double* copyFrom;
        long copyTo;
} AdiLineData;

typedef struct {
    int start, stop;
    AdiLineData* vals;
    double* state;
    Grid_node g;
    int sizej;
    AdiLineData (*dg_adi_dir)(Grid_node, double, int, int, double const *, double*);
    double* scratchpad;
} AdiGridData;

extern void set_num_threads(int);
extern int get_num_threads(void);
extern int dg_adi(Grid_node);
extern int dg_adi_vol(Grid_node);
extern int dg_adi_tort(Grid_node);
extern void dg_transfer_data(AdiLineData * const, double* const, int const, int const, int const);
extern void run_threaded_dg_adi(AdiGridData*, pthread_t*, const int, const int, Grid_node, double*, AdiLineData*, AdiLineData (*dg_adi_dir)(Grid_node, double, int, int, double const *, double*), const int n);

extern ReactGridData* create_threaded_reactions(void);
extern void* do_reactions(void*);


/*Variable set function declarations*/
extern void scatter_concentrations(void);
extern int find(const int, const int, const int, const int, const int);
extern void update_boundaries_x(int, int, int, int, int, double, double, double, int, int, int, const double const*, double*);
extern void update_boundaries_y(int, int, int, int, int, double, double, double, int, int, int, const double const*, double*);
extern void update_boundaries_z(int, int, int, int, int, double, double, double, int, int, int, const double const*, double*);

extern void _rhs_variable_step_helper(Grid_node*, const double const*, double*);
extern void _rhs_variable_step_helper_tort(Grid_node*, const double const*, double*);
extern void _rhs_variable_step_helper_vol(Grid_node*, const double const*, double*);
