from neuron import h, rxd
from matplotlib import pyplot

h.load_file('stdrun.hoc')

sec = h.Section()
sec.nseg = 100
sec.L = 100

r = rxd.Region([sec])
ca = rxd.Species(r, d=1, initial=lambda node: 1 if 0.4 < node.x < 0.6 else 0)

def plot_it():
    pyplot.plot([seg.x * sec.L for seg in sec], ca.states)

h.finitialize()

for i in xrange(5):
    h.continuerun(i * 25)
    plot_it()

pyplot.xlim([0, sec.L])
pyplot.ylim([0, 1])
pyplot.show()
