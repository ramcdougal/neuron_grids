"""
rxdfast module

interconnection between Python and C for NEURON rxd v2
"""

from neuron import nrn_dll_sym, nrn_dll, h
from neuron.rxd.geometry import _surface_areas1d
from neuron.rxd import RxDException
#from neuron.rxd import rxdmath
import rxdmath
import ctypes
import math
import weakref
import numpy
from os import path 

_all_rates = []
set_nonvint_block = nrn_dll_sym('set_nonvint_block')
nrn = nrn_dll()
dll = ctypes.cdll[path.dirname(path.realpath(__file__)) + '/../rxd.so']

fptr_prototype = ctypes.CFUNCTYPE(None)

set_nonvint_block(dll.rxd_nonvint_block)

set_setup = dll.set_setup
set_setup.argtypes = [fptr_prototype]
set_initialize = dll.set_initialize
set_initialize.argtypes = [fptr_prototype]

#setup_solver = dll.setup_solver
#setup_solver.argtypes = [ctypes.py_object, ctypes.py_object, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double]

insert = dll.insert
insert.argtypes = [ctypes.c_int, ctypes.py_object, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.py_object, ctypes.py_object]

insert.restype = ctypes.c_int

make_time_ptr = dll.make_time_ptr
make_time_ptr.argtypes = [ctypes.py_object, ctypes.py_object]

#states = None
_set_num_threads = dll.set_num_threads 
_set_num_threads.argtypes = [ctypes.c_int]
_get_num_threads = dll.get_num_threads
_get_num_threads.restype = ctypes.c_int

_set_grid_concentrations = dll.set_grid_concentrations
_set_grid_concentrations.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.py_object, ctypes.py_object]

_set_grid_currents = dll.set_grid_currents
_set_grid_currents.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.py_object, ctypes.py_object, ctypes.py_object]

# Added for reactions
clear_rates = dll.clear_rates
register_reaction = dll.register_reaction
register_reaction.argtypes = [ctypes.c_int, ctypes.c_int, ctypes._CFuncPtr]

register_subregion_reaction = dll.register_subregion_reaction
register_subregion_reaction.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.POINTER(ctypes.c_ubyte), ctypes._CFuncPtr]


# TODO: replace with a proper singleton
class _Options(object):
    @property
    def num_threads(self):
        return _get_num_threads()
    @num_threads.setter
    def num_threads(self, n):
        _set_num_threads(n)

options = _Options()

_extracellular_diffusion_objects = weakref.WeakKeyDictionary()



region_sec = None
#diffusion_constant_x = 1 # TODO: these are per grid
#diffusion_constant_y = 1
#diffusion_constant_z = 1
#states = None
my_initializer = None

"""
class Node:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

def Region(secs):
    global region_sec
    assert(len(secs) == 1)
    region_sec = secs

class Species:
    def __init__(self, region, dc_x=0, dc_y=0, dc_z=0, initial=None):
        global diffusion_constant_x, diffusion_constant_y, diffusion_constant_z, my_initializer
        assert(region is None)
        diffusion_constant_x = dc_x
        diffusion_constant_y = dc_y
        diffusion_constant_z = dc_z
        my_initializer = initial
    @property
    def states(self):
        return states


Class Grid:
    def __init__(self, size_x, size_y, size_z):
        self.s

"""




class Extracellular:
    """assumes reflective boundary conditions, so you'll probably want to make sure the mesh extends far beyond the neuron
    """
    def __init__(self, xlo, ylo, zlo, xhi, yhi, zhi, dx):
        self._xlo, self._ylo, self._zlo = xlo, ylo, zlo
        self._dx = dx
        self._nx = int(math.ceil((xhi - xlo) / dx))
        self._ny = int(math.ceil((yhi - ylo) / dx))
        self._nz = int(math.ceil((zhi - zlo) / dx))
        self._xhi, self._yhi, self._zhi = xlo + dx * self._nx, ylo + dx * self._ny, zlo + dx * self._nz


class Species(rxdmath._Arithmeticed):
    def __init__(self, region, d=0, name=None, charge=0, initial=0, alpha=0.2, tortuosity=1.0):
        """
            region = Extracellular object (TODO: or list of objects)
            name = string of the name of the NEURON species (e.g. ca)
            d = diffusion constant
            charge = charge of the molecule (used for converting currents to concentration changes)
            initial = initial concentration
            alpha = volume fraction - either a single value for the whole region or a Vector giving a value for each voxel
            tortuosity = increase in path length due to obstacles, effective diffusion coefficient d/tortuosity^2. - either a single value for the whole region or a Vector giving a value for each voxel.
            NOTE: For now, only define this AFTER the morphology is instantiated. In the future, this requirement can be relaxed.
            TODO: remove this limitation

        """
        _extracellular_diffusion_objects[self] = None

        # ensure 3D points exist
        h.define_shape()

        self._regions = [region]
        self._species = name
        self._charge = charge
        self._xlo, self._ylo, self._zlo = region._xlo, region._ylo, region._zlo
        self._dx = region._dx
        self._d = d
        self._nx = region._nx
        self._ny = region._ny
        self._nz = region._nz
        self._xhi, self._yhi, self._zhi = region._xhi, region._yhi, region._zhi
        self._states = h.Vector(self._nx * self._ny * self._nz)
        self.states = self._states.as_numpy().reshape(self._nx, self._ny, self._nz)
        self._initial = initial

        # TODO: Might be difficult for a user to specify alpha & tortuosity
        # they have the grid - should replace this with a separate function
        if(numpy.isscalar(alpha)):
            self._alpha = alpha
            self.alpha = alpha
        else:
            alpha = numpy.array(alpha)
            if(alpha.size != self.states.size):
                 raise RxDException('volume fraction alpha must be a scalar or an array the same size as the grid: {0}x{1}x{2}'.format(self._nx, self._ny, self._nz ))
 
            else:
                self._alpha = h.Vector(self._nx * self._ny * self._nz);
                self.alpha = self._alpha.as_numpy().reshape(self._nx, self._ny, self._nz)
                self.alpha[:] = alpha
                alpha = self._alpha._ref_x[0]

        if(numpy.isscalar(tortuosity)):
            self._tortuosity = tortuosity
            self.tortuosity = tortuosity
        else:
            tortuosity = numpy.array(tortuosity)
            if(tortuosity.size != self.states.size):
                 raise RxDException('tortuosity must be a scalar or an array the same size as the grid: {0}x{1}x{2}'.format(self._nx, self._ny, self._nz ))
    
            else:
                self._tortuosity = h.Vector(self._nx * self._ny * self._nz);
                self.tortuosity = self._tortuosity.as_numpy().reshape(self._nx, self._ny, self._nz)
                self.tortuosity[:] = tortuosity
                tortuosity = self._tortuosity._ref_x[0]


        # TODO: if allowing different diffusion rates in different directions, verify that they go to the right ones
        self._grid_id = insert(0, self._states._ref_x[0], self._nx, self._ny, self._nz, self._d, self._d, self._d, self._dx, self._dx, self._dx, alpha, tortuosity)

        self._str = '_species[%d]' % self._grid_id
        self._name = name


        # set up the ion mechanism and enable active Nernst potential calculations
        self._ion_register()

        self._update_pointers()

    def __del__(self):
        # TODO: remove this object from the list of grids, possibly by reinserting all the others
        # NOTE: be careful about doing the right thing at program's end; some globals may no longer exist
        pass

    @property
    def extent(self):
        return [self._xlo, self._xhi, self._ylo, self._yhi]

    def _finitialize(self):
        # TODO: support more complicated initializations than just constants
        self.states[:] = self._initial

    def _ion_register(self):
        """modified from neuron.rxd.species.Species._ion_register"""
        ion_type = h.ion_register(self._species, self._charge)
        if ion_type == -1:
            raise RxDException('Unable to register species: %s' % self._species)
        # insert the species if not already present
        ion_forms = [self._species + 'i', self._species + 'o', 'i' + self._species, 'e' + self._species]
        for s in h.allsec():
            try:
                for i in ion_forms:
                    # this throws an exception if one of the ion forms is missing
                    temp = s.__getattribute__(i)
            except:
                s.insert(self._species + '_ion')
            # set to recalculate reversal potential automatically
            # the last 1 says to set based on global initial concentrations
            # e.g. nai0_na_ion, etc...
            h.ion_style(self._species + '_ion', 3, 2, 1, 1, 1, sec=s)

    def _nodes_by_location(self, i, j, k):
        return (i * self._ny + j) * self._nz + k

    def index_from_xyz(self, x, y, z):
        """Given an (x, y, z) point, return the index into the _species vector containing it or None if not in the domain"""
        if x < self._xlo or x > self._xhi or y < self._ylo or y > self._yhi or z < self._zlo or z > self._zhi:
            return None
        # if we make it here, then we are inside the domain
        i = int((x - self._xlo) / self._dx)
        j = int((y - self._ylo) / self._dx)
        k = int((z - self._zlo) / self._dx)
        return self._nodes_by_location(i, j, k)

    def ijk_from_index(self, index):
        nynz = self._ny * self._nz
        i = int(index / nynz)
        jk = index - nynz * i
        j = int(jk / self._nz)
        k = jk % self._nz
        # sanity check
        assert(index == self._nodes_by_location(i, j, k))
        return i, j, k

    def xyz_from_index(self, index):
        i, j, k = ijk_from_index(self, index)
        return self._xlo + i * self._dx, self._ylo + j * self._dx, self._zlo + k * self._dx

    def _locate_segments(self):
        """Note: there may be Nones in the result if a section extends outside the extracellular domain

        Note: the current version keeps all the sections alive (i.e. they will never be garbage collected)
        TODO: fix this
        """
        result = {}
        for sec in h.allsec():
            result[sec] = [self.index_from_xyz(*_xyz(seg)) for seg in sec]
        return result

    def _update_pointers(self):
        # TODO: call this anytime the _grid_id changes and anytime the structure_change_count changes
        self._seg_indices = self._locate_segments()

        grid_list = 0
        grid_indices = []
        neuron_pointers = []
        stateo = '_ref_' + self._species + 'o'
        for sec, indices in self._seg_indices.iteritems():
            for seg, i in zip(sec, indices):
                if i is not None:
                    grid_indices.append(i)
                    neuron_pointers.append(seg.__getattribute__(stateo))
        _set_grid_concentrations(grid_list, self._grid_id, grid_indices, neuron_pointers)

        tenthousand_over_charge_faraday = 10000. / (self._charge * h.FARADAY)
        scale_factor = tenthousand_over_charge_faraday / (self._dx ** 3)
        ispecies = '_ref_i' + self._species
        neuron_pointers = []
        scale_factors = []
        for sec, indices in self._seg_indices.iteritems():
            for seg, surface_area, i in zip(sec, _surface_areas1d(sec), indices):
                if i is not None:
                    neuron_pointers.append(seg.__getattribute__(ispecies))
                    scale_factors.append(float(scale_factor * surface_area))
        _set_grid_currents(grid_list, self._grid_id, grid_indices, neuron_pointers, scale_factors)





class Rate:
    def __init__(self, species, rate, subregion=False):
        assert(isinstance(species, Species))
        self._rate = rxdmath._ensure_arithmeticed(rate)
        # TODO: add callback to redo setup if Rate goes out of scope
        _all_rates.append(weakref.ref(self))
        # TODO: this probably shouldn't keep species alive
        self._species = species
        self._compile()

        if isinstance(subregion,bool):
            self._subregion = None
            register_reaction(0, self._species._grid_id, self._compiled)
        else:
            self._subregion = _list_to_uchar_array(subregion.flatten()>0)
            register_subregion_reaction(0, self._species._grid_id, self._subregion, self._compiled)


        
        
    
    def _compile(self):
        self._compiled = self._rate._compile()


def _xyz(seg):
    """Return the (x, y, z) coordinate of the center of the segment."""
    # TODO: this is very inefficient, especially since we're calling this for each segment not for each section; fix
    sec = seg.sec
    n3d = int(h.n3d(sec=sec))
    x3d = [h.x3d(i, sec=sec) for i in xrange(n3d)]
    y3d = [h.y3d(i, sec=sec) for i in xrange(n3d)]
    z3d = [h.z3d(i, sec=sec) for i in xrange(n3d)]
    arc3d = [h.arc3d(i, sec=sec) for i in xrange(n3d)]
    return numpy.interp([seg.x * sec.L], arc3d, x3d)[0], numpy.interp([seg.x], arc3d, y3d)[0], numpy.interp([seg.x], arc3d, z3d)[0]



has_setup = False
def do_setup():
    global has_setup
    print "DO SETUP!"
    make_time_ptr(h._ref_dt, h._ref_t);
    
    has_setup = True


def do_initialize():
    print 'do_initialize called', has_setup
    if has_setup:
        """handle initialization at finitialize time"""
        for obj in _extracellular_diffusion_objects:
            obj._finitialize()
        # TODO: allow

# register the Python callbacks

def _list_to_cint_array(data):
    return (ctypes.c_int * len(data))(*tuple(data))
        
def _list_to_uchar_array(data):
    return (ctypes.c_ubyte * len(data))(*tuple(data))

do_setup_fptr = fptr_prototype(do_setup)
do_initialize_fptr = fptr_prototype(do_initialize)
set_setup(do_setup_fptr)
set_initialize(do_initialize_fptr)



