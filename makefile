#gcc -I/usr/include/python2.7 -lpython2.7 -shared -o rxd.so -fPIC rxd.c 

# Makefile for files: rxd.c , grids.c , grids.h

CC = gcc
CFLAGS = -g3 -std=c99 -pedantic -Wall

all: rxd

rxd: rxd.o grids.o rxd_vol.o
	${CC} ${CFLAGS} -I/usr/include/python2.7 -lpython2.7 -shared grids.o rxd.o rxd_vol.o -o rxd.so 

rxd_vol.o: rxd_vol.c
	${CC} ${CFLAGS} -c -I/usr/include/python2.7 -lpython2.7 -fPIC rxd_vol.c -o rxd_vol.o

rxd.o: rxd.c
	${CC} ${CFLAGS} -c -I/usr/include/python2.7 -lpython2.7 -fPIC rxd.c -o rxd.o

grids.o: grids.c grids.h
	${CC} ${CFLAGS} -c -I/usr/include/python2.7 -lpython2.7 -fPIC grids.c -o grids.o

clean:
	rm -f *.o *.so
